import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import axios from 'axios';
import RandomWord from './components/randomWord.vue';
import OneWord from './components/oneWord.vue';
import Challenge from './components/30days.vue';

Vue.use(Router);
window.axios = require('axios');

Vue.config.productionTip = false;


const routes = [
  {
    path: '/',
    component: RandomWord
  },
  {
    path: '/view',
    component: OneWord
  },
  {
    path: '/30days',
    component: Challenge
  }
  ]

const router = new Router({
  routes
});
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
